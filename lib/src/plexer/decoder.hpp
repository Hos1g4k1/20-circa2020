#ifndef DECODER_H
#define DECODER_H

#include <cmath>

#include "component/component.hpp"

class Decoder : public Component
{

public:
	Decoder();
	~Decoder() override;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;
	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // DECODER_H
