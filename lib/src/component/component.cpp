#include "component.hpp"

#include <algorithm>
#include <unordered_set>

#include "connection/connectionmanager.hpp"

const unsigned Component::OVERFLOW_THRESHOLD;

Component::Component(unsigned n_input_slots, unsigned n_output_slots)
	: input_connections(std::vector<Connection>(n_input_slots, Connection())),
	  output_connections(std::vector<Connection>(n_output_slots, Connection())), m_id(-1),
	  m_nInputs(n_input_slots), m_nOutputs(n_output_slots)
{
}

bool Component::propagateInputs()
{
	bool has_change = false;
	if (m_nInputs != getInputSize() || m_nOutputs != getOutputSize()) {
		m_nInputs = getInputSize();
		m_nOutputs = getOutputSize();
		has_change = true;
	}
	for (auto &connection : input_connections) {
		if (connection)
			connection.setSignal(connection.to_component->output_connections[connection.to_index].getSignal());
		if (connection.hasChanged())
			has_change = true;
		//        if(!connection && connection.getSignal().getState() != Signal::State::UNDEFINED)
		//            throw "Invalid connection state";
	}
	return has_change;
}

void Component::notifyStateChanged()
{
	std::unordered_map<Component *, unsigned> m;
	this->notifyStateChanged(m);
}

void Component::notifyStateChanged(std::unordered_map<Component *, unsigned> &visit_count)
{
	if (propagateInputs()) {
		auto it = visit_count.find(this);
		unsigned my_count;
		if (it == visit_count.end()) {
			my_count = (visit_count[this] = 1);
		} else {
			my_count = ++it->second;
		}
		if (my_count > OVERFLOW_THRESHOLD) {
			std::cerr << "[" << this->short_identify() << "] detected infinite circuit, stopping." << std::endl;
			return;
		}

		update();

		// Mark input connections as not changed
		for (auto &conn : input_connections) {
			conn.flush();
		}

		// Update changed connections
		std::unordered_set<Connection *> updated;
		for (auto &conn : output_connections) {
			if (conn && conn.hasChanged() && updated.find(&conn) == updated.end()) {
				//                conn.to_component->notifyStateChanged(visit_count);
				updated.insert(&conn);
			}
			if (conn.hasChanged())
				conn.flush();
		}

		for (const auto &conn : updated) {
			conn->to_component->notifyStateChanged(visit_count);
		}
	}
}

size_t Component::getInputSize()
{
	return this->input_connections.size();
}

size_t Component::getOutputSize()
{
	return this->output_connections.size();
}

void Component::resizeInputs(unsigned n)
{
	if (n == getInputSize())
		return;
	if (n < getInputSize())
		ConnectionManager::Instance().disconnectInputs(this, n, getInputSize());
	input_connections.resize(n, Connection());
	notifyStateChanged();
}

void Component::resizeOutputs(unsigned n)
{
	if (n == getOutputSize())
		return;
	if (n < getOutputSize())
		ConnectionManager::Instance().disconnectOutputs(this, n, getOutputSize());
	output_connections.resize(n, Connection());
	notifyStateChanged();
}

// TODO Remove
std::string Component::short_identify()
{
	std::string tp;
	switch (getType()) {
	case ComponentTypes::WIRE:
		tp = "WIRE";
		break;
	case ComponentTypes::PIN:
		tp = "PIN";
		break;
	case ComponentTypes::LIGHTBULB:
		tp = "LB";
		break;
	case ComponentTypes::CLOCK:
		tp = "CLOCK";
		break;
	case ComponentTypes::ANDGATE:
		tp = "AND";
		break;
	case ComponentTypes::ORGATE:
		tp = "OR";
		break;
	case ComponentTypes::NOTGATE:
		tp = "NOT";
		break;
	case ComponentTypes::XORGATE:
		tp = "XOR";
		break;
	case ComponentTypes::CONSTANT:
		tp = "CONSTANT";
		break;
	case ComponentTypes::COMPOSITE:
		tp = "COMP";
		break;
	case ComponentTypes::NANDGATE:
		tp = "NAND";
		break;
	case ComponentTypes::NORGATE:
		tp = "NOR";
		break;
	case ComponentTypes::XNORGATE:
		tp = "XNOR";
		break;
	case ComponentTypes::BUFFER:
		tp = "BUFFER";
		break;
	case ComponentTypes::MULTIPLEXER:
		tp = "MULTIPLEXER";
		break;
	case ComponentTypes::DEMULTIPLEXER:
		tp = "DEMULTIPLEXER";
		break;
	case ComponentTypes::ENCODER:
		tp = "ENCODER";
		break;
	case ComponentTypes::DECODER:
		tp = "DECODER";
		break;
	case ComponentTypes::COMPARATOR:
		tp = "COMPARATOR";
		break;
	case ComponentTypes::ADDER:
		tp = "ADDER";
		break;
	case ComponentTypes::SUBTRACTOR:
		tp = "SUBTRACTOR";
		break;
	case ComponentTypes::MULTIPLIER:
		tp = "MULTIPLIER";
		break;
	case ComponentTypes::DIVIDER:
		tp = "DIVIDER";
		break;
	case ComponentTypes::NEGATOR:
		tp = "NEGATOR";
		break;
	case ComponentTypes::SPLITTER:
		tp = "SPLITTER";
		break;
	case ComponentTypes::SRFLIPFLOP:
		tp = "SRFLIPFLOP";
		break;
	case ComponentTypes::JKFLIPFLOP:
		tp = "JKFLIPFLOP";
		break;
	case ComponentTypes::TFLIPFLOP:
		tp = "TFLIPFLOP";
		break;
	case ComponentTypes::DFLIPFLOP:
		tp = "DFLIPFLOP";
		break;
	}

	std::ostringstream s;
	s << (void const *)this;
	std::string addr = s.str();
	std::reverse(addr.begin(), addr.end());
	addr = addr.substr(0, 6);
	std::reverse(addr.begin(), addr.end());
	return tp + "(" + addr + ")";
}

bool Component::input_connection_taken(unsigned idx)
{
	return this->input_connections[idx];
}

bool Component::output_connection_taken(unsigned idx)
{
	return this->output_connections[idx];
}

std::vector<Connection> &Component::getInputConnections()
{
	return input_connections;
}

std::vector<Connection> &Component::getOutputConnections()
{
	return output_connections;
}

// Should these getters return Signal or Signal::State?
Signal Component::getInput(unsigned idx)
{
	return this->input_connections[idx].getSignal();
}

Signal Component::getOutput(unsigned idx)
{
	return this->output_connections[idx].getSignal();
}

void Component::setInput(unsigned index, Signal::State state)
{
	if (index >= getInputSize()) {
		throw "Index out of range";
		return;
	}
	input_connections[index].setSignal(state);
}

void Component::setId(int id)
{
	m_id = id;
}

int Component::getId() const
{
	return m_id;
}

void Component::serialize(std::ostream &os) const
{
	size_t nInputs = input_connections.size();
	os.write((char *)(&nInputs), sizeof(nInputs));
	for (const auto &connection : input_connections) {
		connection.serialize(os);
	}

	size_t nOutputs = output_connections.size();
	os.write((char *)(&nOutputs), sizeof(nOutputs));
	for (const auto &connection : output_connections) {
		connection.serialize(os);
	}
}

void Component::deserialize(std::istream &is, std::map<int, Component *> lookup)
{
	size_t nInputs;
	is.read((char *)(&nInputs), sizeof(nInputs));
	input_connections.resize(nInputs);
	for (size_t i = 0; i < nInputs; i++) {
		input_connections[i].deserialize(is, lookup);
	}

	size_t nOutputs;
	is.read((char *)(&nOutputs), sizeof(nOutputs));
	output_connections.resize(nOutputs);
	for (size_t i = 0; i < nOutputs; i++) {
		output_connections[i].deserialize(is, lookup);
	}
}
