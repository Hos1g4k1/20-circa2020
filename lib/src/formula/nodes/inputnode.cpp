#include "inputnode.hpp"

InputNode::InputNode(int id) : m_id(id)
{
}

InputNode::~InputNode() = default;

std::string InputNode::toString() const
{
	return "pin[" + std::to_string(m_id) + "]";
}

OpNode *InputNode::simplify()
{
	return new InputNode(m_id);
}

int InputNode::getId() const
{
	return m_id;
}

OpType InputNode::getType() const
{
	return OpType::INPUT;
}

bool InputNode::isEqual(OpNode *other) const
{
	auto *node = static_cast<InputNode *>(other);
	return m_id == node->getId();
}
