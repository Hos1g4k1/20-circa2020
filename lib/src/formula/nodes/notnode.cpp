#include "notnode.hpp"

#include <sstream>

#include "constantnode.hpp"
#include "util/stringutil.hpp"

NotNode::NotNode(OpNode *input) : m_input(input)
{
}

NotNode::~NotNode()
{
	delete m_input;
}

std::string NotNode::toString() const
{
	std::ostringstream os;
	os << "!"
	   << StringUtil::parenthesize_if(
			  m_input->toString(), getPriority(m_input->getType()) > getPriority(getType()));
	return os.str();
}

OpNode *NotNode::simplify()
{
	OpNode *simplified = m_input->simplify();
	if (simplified->getType() == OpType::CONSTANT) {
		auto *n = static_cast<ConstantNode *>(simplified);
		bool value = n->getValue();
		delete simplified;
		return new ConstantNode(!value);
	} else if (simplified->getType() == OpType::NOT) {
		auto *n = static_cast<NotNode *>(simplified);
		OpNode *replacement = n->getInput()->simplify();
		delete simplified;
		return replacement;
	}
	return new NotNode(m_input->simplify());
}

OpNode *NotNode::getInput() const
{
	return m_input;
}

OpType NotNode::getType() const
{
	return OpType::NOT;
}

bool NotNode::isEqual(OpNode *other) const
{
	auto *node = static_cast<NotNode *>(other);
	return equals(m_input, node->getInput());
}
