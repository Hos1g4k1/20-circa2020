#ifndef CONSTANTNODE_H
#define CONSTANTNODE_H

#include <string>

#include "opnode.hpp"

class ConstantNode : public OpNode
{
public:
	ConstantNode(bool input);
	~ConstantNode() override;

	std::string toString() const override;
	OpNode *simplify() override;

	bool getValue() const;

	OpType getType() const override;

protected:
	bool isEqual(OpNode *other) const override;

private:
	bool m_input;
};

#endif // CONSTANTNODE_H
