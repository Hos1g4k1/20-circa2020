#ifndef INPUTNODE_H
#define INPUTNODE_H

#include "opnode.hpp"

class InputNode : public OpNode
{
public:
	InputNode(int id);
	~InputNode() override;

	std::string toString() const override;
	OpNode *simplify() override;

	int getId() const;

	OpType getType() const override;

protected:
	bool isEqual(OpNode *other) const override;

private:
	int m_id;
};

#endif // INPUTNODE_H
