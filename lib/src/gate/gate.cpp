#include "gate.hpp"

Gate::Gate(unsigned n_input_slots) : Component(n_input_slots, 1)
{
}

Gate::~Gate() = default;

void Gate::update()
{
	Signal new_output = this->op();
	this->output_connections[0].setSignal(new_output);
}

void Gate::resizeInputs(unsigned int n)
{
	if (n == 1)
		throw "Gate must have at least one input!";
	Component::resizeInputs(n);
}

void Gate::resizeOutputs(unsigned int n)
{
	if (n != 1)
		throw "Gates must have just one output!";
}
