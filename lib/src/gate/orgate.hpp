#ifndef ORGATE_H
#define ORGATE_H

#include "gate.hpp"

class OrGate : public Gate
{
public:
	OrGate(unsigned nInputs = 2);
	~OrGate() override;

	ComponentTypes getType() const override;

protected:
	Signal op() override;
};

#endif // ORGATE_H
