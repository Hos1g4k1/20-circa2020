#include "xnorgate.hpp"

#include <algorithm>

XnorGate::XnorGate(unsigned nInputs) : Gate(nInputs)
{
}

XnorGate::~XnorGate() = default;

Signal XnorGate::op()
{

	if (std::any_of(std::begin(input_connections), std::end(input_connections), [](const Connection s) {
			return s.getSignal().getState() == Signal::State::ERROR;
		}))
		return Signal(Signal::State::ERROR);

	if (std::any_of(std::begin(input_connections), std::end(input_connections), [](const Connection &s) {
			return s.getSignal().getState() == Signal::State::UNDEFINED;
		}))
		return Signal(Signal::State::UNDEFINED);

	auto _true =
		std::count_if(std::begin(input_connections), std::end(input_connections), [](const Connection &s) {
			return s.getSignal().getState() == Signal::State::TRUE;
		});

	if (_true % 2 == 0)
		return Signal(Signal::State::TRUE);

	return Signal(Signal::State::FALSE);
}

ComponentTypes XnorGate::getType() const
{
	return ComponentTypes::XNORGATE;
}
