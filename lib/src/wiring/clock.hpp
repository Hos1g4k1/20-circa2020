#ifndef CLOCK_H
#define CLOCK_H

#include "component/component.hpp"

class Clock : public Component
{
public:
	Clock(const Signal::State &);
	Clock();
	~Clock() override;

	void tick();
	void setState(Signal::State);
	Signal::State getState();
	void resizeOutputs(unsigned n) override;
	void resizeInputs(unsigned n) override;
	void update() override;

	void serialize(std::ostream &os) const override;
	void deserialize(std::istream &is, std::map<int, Component *> lookup) override;

	ComponentTypes getType() const override;

private:
	Signal::State m_state;
};

#endif // CLOCK_H
