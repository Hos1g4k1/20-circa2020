# Circa2020

Tool for designing and simulating logic circuits

For information and instructions look at the [wiki](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/20-circa2020/-/wikis/home)

## Features

- Components:
    - Wiring (Pin, Lightbulb, Splitter, Clock)
    - Basic logic gates (NOT, AND, OR, XOR, NAND, NOR, ...)
    - Plexers (Multiplxer, Demultiplexer, Encoder, Decoder)
    - Arithmetic (Adder, Multiplier, ...)
    - Memory (D, T, JK, SR flip-flop)
- Saving and opening circuit boards
- Resizing inputs and outputs
- Copy / Paste
- Creating components from selections
- Converting a circuit into a logic formula
- Rotating single components
- Deleting single components and selections
- Overlap detection

## Controls

| Key        | Action                                    |
| ---------- | ----------------------------------------- |
| **d**      | Switch to design mode                     |
| **s**      | Switch to selection mode                  |
| **p**      | Switch to pin flip mode                   |
| **ctrl+a** | Select all                                |
| **ctrl+c** | Copy selection (only in selection mode)   |
| **ctrl+v** | Paste selection (only in selection mode)  |
| **delete** | Delete selection (only in selection mode) |
| **ctrl+n** | Create new circuit board                  |
| **ctrl+s** | Save circuit board                        |
| **LMB**    | Select / Connect                          |
| **RMB**    | Delete component / selection              |

## Screenshots

![](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/20-circa2020/-/wikis/uploads/c2cbfadba98650dbbef3d59ed1eed60e/img_3.png)
![](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/20-circa2020/-/wikis/uploads/e06e25b3277dd9c083c87b55b2f5cf96/img_5.png)
![](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/20-circa2020/-/wikis/uploads/7c39d8702c41c33f09a41a0559234d66/img_4.png)
![](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/20-circa2020/-/wikis/uploads/22b72067a6c086d6785dc68af4f453c9/img_1.png)
![](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/20-circa2020/-/wikis/uploads/5272dfccbf1cab8f60877dadf856929d/img_2.png)

## Requirements
- C++17 or greater
- git
- cmake
- Qt5

## Building the project
After cloning the project and navigating to it run the following commands:
```bash
mkdir build
cd build
cmake -g "Unix Makefiles" -DCMAKE_PREFIX_PATH="/usr/lib64/cmake" ..
make Circa2020
```
where `/usr/lib64/cmake` is the path to Qt5 cmake libraries

*Note: If your Qt5 cmake libraries don't share the same base directory you will need to set them separately using `-DQt5Core_DIR`, `-DQt5Gui_DIR`, `-DQt5Widgets_DIR` and `-DQt5_DIR` instead of `-DCMAKE_PREFIX_PATH`*

## Building and running tests
Building the tests doesn't require any libraries to be installed. Catch2 will be automatically downloaded when cmake is run.
```bash
mkdir build
cd build
cmake -g "Unix Makefiles" ..
make Circa2020_test
```
Then you can run the tests normally using
```bash
./Circa2020_test
```
or to generate a report in xml format
```bash
./Circa2020_test -r junit -o report.xml
```

## Developers

- [Aleksa Kojadinović, 130/2017](https://gitlab.com/aleksakojadinovic)
- [Vladimir Vuksanović, 145/2017](https://gitlab.com/VladimirV99)
- [Tatjana Kunić, 139/2017](https://gitlab.com/kunict11)
- [Lazar Čeliković, 259/2017](https://gitlab.com/Hos1g4k1)
