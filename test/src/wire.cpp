#include "wiring/wire.hpp"

#include "catch.hpp"
#include "component/componentfactory.hpp"
#include "connection/connectionmanager.hpp"
#include "wiring/lightbulb.hpp"
#include "wiring/pin.hpp"

TEST_CASE("Wire Constructor", "[wire][init][unit]")
{
	Wire wire;
	REQUIRE(wire.getInputSize() == 6);
	REQUIRE(wire.getOutputSize() == 6);
	REQUIRE(wire.getState() == Signal::State::UNDEFINED);
}

TEST_CASE("Wire Update", "[wire][update][integration]")
{
	SECTION("One way connection", "Connection to inputs and outputs")
	{
		Pin *pin = ComponentFactory::instance().makePin();
		Wire *wire = ComponentFactory::instance().makeWire();
		LightBulb *lightbulb = ComponentFactory::instance().makeLightBulb();

		ConnectionManager::Instance().connect(pin, 0, wire, 0);
		ConnectionManager::Instance().connect(wire, 1, lightbulb, 0);

		REQUIRE(lightbulb->getOutput() == Signal(Signal::State::UNDEFINED));

		pin->setState(Signal::State::ERROR);
		REQUIRE(lightbulb->getOutput() == Signal(Signal::State::ERROR));

		pin->setState(Signal::State::FALSE);
		REQUIRE(lightbulb->getOutput() == Signal(Signal::State::FALSE));

		pin->flip();
		REQUIRE(lightbulb->getOutput() == Signal(Signal::State::TRUE));

		pin->flip();
		REQUIRE(lightbulb->getOutput() == Signal(Signal::State::FALSE));
	}

	SECTION("Two way connection", "Connection to other wire")
	{
		Pin *pin1 = ComponentFactory::instance().makePin(Signal::State::UNDEFINED);
		Pin *pin2 = ComponentFactory::instance().makePin(Signal::State::UNDEFINED);
		Wire *w1 = ComponentFactory::instance().makeWire();
		Wire *w2 = ComponentFactory::instance().makeWire();
		Wire *w3 = ComponentFactory::instance().makeWire();
		LightBulb *lb = ComponentFactory::instance().makeLightBulb();

		ConnectionManager::Instance().connect(pin1, 0, w1, 0);
		ConnectionManager::Instance().connect(pin2, 0, w2, 0);

		ConnectionManager::Instance().connect(w1, 1, w2, 1);
		ConnectionManager::Instance().connect(w1, 2, w3, 1);
		ConnectionManager::Instance().connect(w2, 2, w3, 2);

		ConnectionManager::Instance().connect(w3, 0, lb, 0);

		REQUIRE(lb->getOutput() == Signal(Signal::State::UNDEFINED));

		pin1->setState(Signal::State::FALSE);
		REQUIRE(lb->getOutput() == Signal(Signal::State::FALSE));

		pin2->setState(Signal::State::TRUE);
		REQUIRE(lb->getOutput() == Signal(Signal::State::ERROR));

		pin1->setState(Signal::State::TRUE);
		REQUIRE(lb->getOutput() == Signal(Signal::State::TRUE));

		pin2->setState(Signal::State::ERROR);
		REQUIRE(lb->getOutput() == Signal(Signal::State::ERROR));

		pin2->setState(Signal::State::UNDEFINED);
		REQUIRE(lb->getOutput() == Signal(Signal::State::TRUE));
	}
}

TEST_CASE("Wire Resize Inputs", "[wire][resize][unit]")
{
	Wire wire;

	SECTION("Resize smaller", "Decrease number of connections")
	{
		wire.resizeInputs(4);
		REQUIRE(wire.getInputSize() == 4);
		REQUIRE(wire.getOutputSize() == 4);
	}

	SECTION("Resize larger", "Increase number of connections")
	{
		wire.resizeInputs(8);
		REQUIRE(wire.getInputSize() == 8);
		REQUIRE(wire.getOutputSize() == 8);
	}
}

TEST_CASE("Wire Resize Outputs", "[wire][resize][unit]")
{
	Wire wire;

	SECTION("Resize smaller", "Decrease number of connections")
	{
		wire.resizeOutputs(4);
		REQUIRE(wire.getInputSize() == 4);
		REQUIRE(wire.getOutputSize() == 4);
	}

	SECTION("Resize larger", "Increase number of connections")
	{
		wire.resizeOutputs(8);
		REQUIRE(wire.getInputSize() == 8);
		REQUIRE(wire.getOutputSize() == 8);
	}
}
