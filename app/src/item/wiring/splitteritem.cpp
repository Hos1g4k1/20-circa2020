#include "splitteritem.hpp"

SplitterItem::SplitterItem(int posX, int posY, QGraphicsItem *parent)
	: SplitterItem(ComponentFactory::instance().makeSplitter(), posX, posY, parent)
{
}

SplitterItem::SplitterItem(Component *component, int posX, int posY, QGraphicsItem *parent)
	: ComponentGraphicsItem(posX, posY, 0, 0, component, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::SPLITTER)
		throw "Incompatible component for this item";
	m_isResizable = true;
	this->resizeInputs(m_component->getOutputSize());
}

int SplitterItem::calculateHeight() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT)
		return m_component->getOutputSize() * 2;
	else
		return 3;
}

int SplitterItem::calculateWidth() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT)
		return 3;
	else
		return m_component->getOutputSize() * 2;
}

void SplitterItem::drawOrientation()
{
	prepareGeometryChange();

	m_height = calculateHeight() * Scene::UNIT;
	m_width = calculateWidth() * Scene::UNIT;

	calculatePath();
	m_inputPoints = {};
	m_outputPoints = {};

	if (m_orientation == Direction::RIGHT) {
		m_inputPoints = {ConnectionPoint(0, m_height / 2, Direction::LEFT)};

		for (unsigned i = 0; i < m_component->getOutputSize(); i++)
			m_outputPoints.append(ConnectionPoint(m_width, (2 * i + 1) * Scene::UNIT, Direction::RIGHT));

	} else if (m_orientation == Direction::DOWN) {
		m_inputPoints = {ConnectionPoint(m_width / 2, 0, Direction::UP)};

		for (unsigned i = 0; i < m_component->getOutputSize(); i++)
			m_outputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, m_height, Direction::DOWN));

	} else if (m_orientation == Direction::LEFT) {
		m_inputPoints = {ConnectionPoint(m_width, m_height / 2, Direction::RIGHT)};

		for (unsigned i = 0; i < m_component->getOutputSize(); i++)
			m_outputPoints.append(ConnectionPoint(0, (2 * i + 1) * Scene::UNIT, Direction::LEFT));

	} else {
		m_inputPoints = {ConnectionPoint(m_width / 2, m_height, Direction::DOWN)};

		for (unsigned i = 0; i < m_component->getOutputSize(); i++)
			m_outputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, 0, Direction::UP));
	}
}

void SplitterItem::resizeInputs(unsigned n)
{
	this->m_component->resizeOutputs(n);
	drawOrientation();
}

void SplitterItem::calculatePath()
{
	if (m_orientation == Direction::RIGHT) {
		m_path = QPainterPath(QPoint(0, m_height / 2));
		m_path.lineTo(m_width, m_height / 2);
		m_path.lineTo(m_width, 0);
		m_path.lineTo(m_width, m_height);
	} else if (m_orientation == Direction::DOWN) {
		m_path = QPainterPath(QPoint(m_width / 2, 0));
		m_path.lineTo(m_width / 2, m_height);
		m_path.lineTo(0, m_height);
		m_path.lineTo(m_width, m_height);
	} else if (m_orientation == Direction::LEFT) {
		m_path = QPainterPath(QPoint(m_width, m_height / 2));
		m_path.lineTo(0, m_height / 2);
		m_path.lineTo(0, 0);
		m_path.lineTo(0, m_height);
	} else {
		m_path = QPainterPath(QPoint(m_width / 2, m_height));
		m_path.lineTo(m_width / 2, 0);
		m_path.lineTo(0, 0);
		m_path.lineTo(m_width, 0);
	}
}

void SplitterItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	// Bounds
	//    painter->setPen(Qt::yellow);
	//    painter->drawRect(boundingRect());

	// Shape
	QPen p = QPen();
	p.setWidth(2);
	painter->setPen(p);
	QBrush b = QBrush(Qt::white);

	painter->drawPath(m_path);

	ComponentGraphicsItem::paint(painter, option, widget);
}
