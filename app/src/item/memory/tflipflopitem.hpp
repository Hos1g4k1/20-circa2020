#ifndef TFLIPFLOPITEM_H
#define TFLIPFLOPITEM_H

#include "item/componentgraphicsitem.hpp"
#include "util/direction.hpp"

class TFlipFlopItem : public ComponentGraphicsItem
{
public:
	TFlipFlopItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
	TFlipFlopItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n);
	void drawOrientation() override;

protected:
	int calculateWidth() const;
	int calculateHeight() const;
	void calculatePath();

	QPainterPath m_path;
};

#endif // TFLIPFLOPITEM_H
