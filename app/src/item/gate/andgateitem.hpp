#ifndef ANDGATEITEM_H
#define ANDGATEITEM_H

#include "gateitem.hpp"

class AndGateItem : public GateItem
{
public:
	AndGateItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
	AndGateItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

protected:
	int calculateWidth() const override;
	int calculateHeight() const override;
	void calculatePath() override;
};

#endif // ANDGATEITEM_H
