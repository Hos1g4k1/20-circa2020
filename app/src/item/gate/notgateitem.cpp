#include "item/gate/notgateitem.hpp"

NotGateItem::NotGateItem(int posX, int posY, QGraphicsItem *parent)
	: NotGateItem(ComponentFactory::instance().makeNotGate(), posX, posY, parent)
{
}

NotGateItem::NotGateItem(Component *component, int posX, int posY, QGraphicsItem *parent)
	: GateItem(component, posX, posY, 4, 2, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::NOTGATE)
		throw "Incompatible component for this item";
	m_isResizable = false;
	this->resizeInputs(m_component->getInputSize());
}

int NotGateItem::calculateHeight() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT)
		return m_component->getInputSize() * 2;
	else
		return int(calculateWidth() * Scene::UNIT * 1.5) / Scene::UNIT + 1;
}

int NotGateItem::calculateWidth() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT)
		return int(m_height * 1.5) / Scene::UNIT + 1;
	else
		return m_component->getInputSize() * 2;
}

void NotGateItem::calculatePath()
{
	if (m_orientation == Direction::RIGHT) {
		m_path = QPainterPath(QPoint(0, 0));
		m_path.lineTo(5 * m_width / 8, m_height / 2);
		m_path.lineTo(0, m_height);
		m_path.lineTo(0, 0);
		m_path.addEllipse(5 * m_width / 8, m_height / 4, Scene::UNIT, Scene::UNIT);
	} else if (m_orientation == Direction::DOWN) {
		m_path = QPainterPath(QPoint(0, 0));
		m_path.lineTo(m_width, 0);
		m_path.lineTo(m_width / 2, 5 * m_height / 8);
		m_path.lineTo(0, 0);
		m_path.addEllipse(m_width / 4, 5 * m_height / 8, Scene::UNIT, Scene::UNIT);
	} else if (m_orientation == Direction::LEFT) {
		m_path = QPainterPath(QPoint(m_width, 0));
		m_path.lineTo(m_width, m_height);
		m_path.lineTo(2 * m_width / 8, m_height / 2);
		m_path.lineTo(m_width, 0);
		m_path.addEllipse(0, m_height / 4, Scene::UNIT, Scene::UNIT);
	} else {
		m_path = QPainterPath(QPoint(0, m_height));
		m_path.lineTo(m_width, m_height);
		m_path.lineTo(m_width / 2, 2 * m_height / 8);
		m_path.lineTo(0, m_height);
		m_path.addEllipse(m_width / 4, 0, Scene::UNIT, Scene::UNIT);
	}
}
