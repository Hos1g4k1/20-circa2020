#ifndef MISC_H
#define MISC_H

#include <functional>
#include <optional>
#include <set>
#include <stack>
#include <unordered_set>
#include <vector>

#include <QGraphicsItem>
#include <QPointF>
#include <QRectF>

#include "direction.hpp"

class Component;
class ComponentGraphicsItem;
class Scene;
class WireItem;
class Wire;

using ShouldConsiderPredicate = std::function<bool(ComponentGraphicsItem *)>;

class MiscUtils
{
public:
	// -----------------OVERLAPS-----------------------------------------------------------------------------------------------------------------------------------------
	static const ShouldConsiderPredicate OVERLAP_CONSIDER_ALL;
	static const ShouldConsiderPredicate OVERLAP_EXCLUDE_WIRES;
	static constexpr auto ItemSortPredicate = [](QGraphicsItem *a, QGraphicsItem *b) {
		if (a->scenePos().y() == b->scenePos().y())
			return a->scenePos().x() < b->scenePos().x();
		return a->scenePos().y() < b->scenePos().y();
	};

	static ShouldConsiderPredicate OVERLAP_EXCLUDE_THIS_ITEM(ComponentGraphicsItem *item);
	static ShouldConsiderPredicate OVERLAP_EXCLUDE_THIS_COMPONENT(Component *c);
	static ShouldConsiderPredicate OVERLAP_EXCLUDE_WIRES_CROSS(const Direction &dir);

	static bool
	overlapsAnyItem(Scene *scene, const QRectF &bounds, const std::vector<ShouldConsiderPredicate> &preds);
	static bool overlapsAnyItem(Scene *scene, const QRectF &bounds, const ShouldConsiderPredicate &pred);
	static bool overlapsAnyItem(Scene *scene, const QRectF &bounds);

	static bool checkPotentialPosition(
		Scene *scene, ComponentGraphicsItem *item, int deltaX, int deltaY,
		const std::vector<ShouldConsiderPredicate> &preds);
	static bool checkPotentialPosition(
		Scene *scene, ComponentGraphicsItem *item, int deltaX, int deltaY, const ShouldConsiderPredicate &pred);
	static bool checkPotentialPosition(Scene *scene, ComponentGraphicsItem *item, int deltaX, int deltaY);
	// ------------------------------------------------------------------------------------------------------------------------------------------------------------------
	static std::optional<QPointF> getGridDelta(const QPointF &previous, const QPointF &current);
	static std::optional<QPointF> getClosestAvailablePosition(Scene *scene, const QRectF &rect);
	static std::optional<QPointF> getClosestAvailablePosition(
		Scene *scene, const QRectF &rect, const std::vector<ShouldConsiderPredicate> &preds);

	static QRectF getGroupRect(const std::unordered_set<ComponentGraphicsItem *> &);
	static QPointF getAnchor(const std::unordered_set<ComponentGraphicsItem *> &);
};

#endif // MISC_H
