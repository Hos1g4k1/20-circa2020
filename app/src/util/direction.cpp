#include "direction.hpp"

bool DirectionUtil::sameOrientation(const Direction &d1, const Direction &d2)
{
	return (d1 == d2 || reverse(d1) == d2);
}

Direction DirectionUtil::reverse(const Direction &d)
{
	if (d == Direction::LEFT) {
		return Direction::RIGHT;
	} else if (d == Direction::RIGHT) {
		return Direction::LEFT;
	} else if (d == Direction::UP) {
		return Direction::DOWN;
	} else {
		return Direction::UP;
	}
}

bool DirectionUtil::horizontal(const Direction &d)
{
	return d == Direction::LEFT || d == Direction::RIGHT;
}

bool DirectionUtil::vertical(const Direction &d)
{
	return !DirectionUtil::horizontal(d);
}

int DirectionUtil::toInt(Direction direction)
{
	switch (direction) {
	case Direction::RIGHT:
		return 0;
	case Direction::LEFT:
		return 1;
	case Direction::UP:
		return 2;
	case Direction::DOWN:
		return 3;
	}
	throw "Invalid direction value";
}

Direction DirectionUtil::fromInt(int d)
{
	switch (d) {
	case 0:
		return Direction::RIGHT;
	case 1:
		return Direction::LEFT;
	case 2:
		return Direction::UP;
	case 3:
		return Direction::DOWN;
	}
	throw "Invalid direction value";
}

void DirectionUtil::serialize(std::ostream &os, Direction direction)
{
	int d = toInt(direction);
	os.write((char *)(&d), sizeof(d));
}

Direction DirectionUtil::deserialize(std::istream &is)
{
	int d;
	is.read((char *)(&d), sizeof(d));
	return fromInt(d);
}
