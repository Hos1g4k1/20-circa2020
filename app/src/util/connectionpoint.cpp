#include "util/connectionpoint.hpp"

ConnectionPoint::ConnectionPoint() : ConnectionPoint(0, 0, Direction::UP)
{
}

ConnectionPoint::ConnectionPoint(int x, int y, Direction direction) : ConnectionPoint(x, y, 8, direction)
{
}

ConnectionPoint::ConnectionPoint(int x, int y, unsigned size, Direction direction)
	: m_focus(false), m_center(x, y), m_bounds(x - (int)size / 2, y - (int)size / 2, size, size),
	  m_direction(direction), m_size(size)
{
}

QPointF ConnectionPoint::getCenter() const
{
	return m_center;
}

QRectF ConnectionPoint::getBounds() const
{
	return m_bounds;
}

Direction ConnectionPoint::getDirection() const
{
	return m_direction;
}

bool ConnectionPoint::hasFocus() const
{
	return m_focus;
}

void ConnectionPoint::setFocus(bool focus)
{
	m_focus = focus;
}

unsigned ConnectionPoint::getSize() const
{
	return m_size;
}
