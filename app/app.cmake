set(SRC_PATH    "${APP_PATH}/src")
set(UI_PATH     "${APP_PATH}/src/ui")
set(RES_PATH    "${APP_PATH}/resources")

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(QT NAMES Qt6 Qt5 COMPONENTS Widgets REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Widgets REQUIRED)

# Application
set(SOURCES
    "${SRC_PATH}/item/gate/andgateitem.cpp"
    "${SRC_PATH}/item/gate/gateitem.cpp"
    "${SRC_PATH}/item/gate/bufferitem.cpp"
    "${SRC_PATH}/item/gate/nandgateitem.cpp"
    "${SRC_PATH}/item/gate/norgateitem.cpp"
    "${SRC_PATH}/item/gate/notgateitem.cpp"
    "${SRC_PATH}/item/gate/orgateitem.cpp"
    "${SRC_PATH}/item/gate/xnorgateitem.cpp"
    "${SRC_PATH}/item/gate/xorgateitem.cpp"
    "${SRC_PATH}/item/arithmetic/comparatoritem.cpp"
    "${SRC_PATH}/item/arithmetic/adderitem.cpp"
    "${SRC_PATH}/item/arithmetic/subtractoritem.cpp"
    "${SRC_PATH}/item/arithmetic/divideritem.cpp"
    "${SRC_PATH}/item/arithmetic/multiplieritem.cpp"
    "${SRC_PATH}/item/arithmetic/negatoritem.cpp"
    "${SRC_PATH}/item/memory/dflipflopitem.cpp"
    "${SRC_PATH}/item/memory/tflipflopitem.cpp"
    "${SRC_PATH}/item/memory/srflipflopitem.cpp"
    "${SRC_PATH}/item/memory/jkflipflopitem.cpp"
    "${SRC_PATH}/item/plexer/encoderitem.cpp"
    "${SRC_PATH}/item/plexer/decoderitem.cpp"
    "${SRC_PATH}/item/plexer/demultiplexeritem.cpp"
    "${SRC_PATH}/item/plexer/multiplexeritem.cpp"
    "${SRC_PATH}/item/wiring/constantitem.cpp"
    "${SRC_PATH}/item/wiring/lightbulbitem.cpp"
    "${SRC_PATH}/item/wiring/pinitem.cpp"
    "${SRC_PATH}/item/wiring/wireitem.cpp"
    "${SRC_PATH}/item/wiring/splitteritem.cpp"
    "${SRC_PATH}/item/wiring/clockitem.cpp"
    "${SRC_PATH}/item/componentgraphicsitem.cpp"
    "${SRC_PATH}/item/compositeitem.cpp"
    "${SRC_PATH}/item/itemfactory.cpp"
    "${SRC_PATH}/util/colorutil.cpp"
    "${SRC_PATH}/util/direction.cpp"
    "${SRC_PATH}/util/connectionpoint.cpp"
    "${SRC_PATH}/util/wiredrawutils.cpp"
    "${SRC_PATH}/util/misc.cpp"
    "${SRC_PATH}/main.cpp"
    "${SRC_PATH}/mainwindow.cpp"
    "${SRC_PATH}/formuladialog.cpp"
    "${SRC_PATH}/scene.cpp"
)

set(HEADERS
    "${SRC_PATH}/item/gate/andgateitem.hpp"
    "${SRC_PATH}/item/gate/bufferitem.hpp"
    "${SRC_PATH}/item/gate/gateitem.hpp"
    "${SRC_PATH}/item/gate/nandgateitem.hpp"
    "${SRC_PATH}/item/gate/norgateitem.hpp"
    "${SRC_PATH}/item/gate/notgateitem.hpp"
    "${SRC_PATH}/item/gate/orgateitem.hpp"
    "${SRC_PATH}/item/gate/xnorgateitem.hpp"
    "${SRC_PATH}/item/gate/xorgateitem.hpp"
    "${SRC_PATH}/item/arithmetic/comparatoritem.hpp"
    "${SRC_PATH}/item/arithmetic/adderitem.hpp"
    "${SRC_PATH}/item/arithmetic/subtractoritem.hpp"
    "${SRC_PATH}/item/arithmetic/divideritem.hpp"
    "${SRC_PATH}/item/arithmetic/multiplieritem.hpp"
    "${SRC_PATH}/item/arithmetic/negatoritem.hpp"
    "${SRC_PATH}/item/memory/dflipflopitem.hpp"
    "${SRC_PATH}/item/memory/tflipflopitem.hpp"
    "${SRC_PATH}/item/memory/srflipflopitem.hpp"
    "${SRC_PATH}/item/memory/jkflipflopitem.hpp"
    "${SRC_PATH}/item/plexer/encoderitem.hpp"
    "${SRC_PATH}/item/plexer/decoderitem.hpp"
    "${SRC_PATH}/item/plexer/multiplexeritem.hpp"
    "${SRC_PATH}/item/plexer/demultiplexeritem.hpp"
    "${SRC_PATH}/item/wiring/constantitem.hpp"
    "${SRC_PATH}/item/wiring/lightbulbitem.hpp"
    "${SRC_PATH}/item/wiring/pinitem.hpp"
    "${SRC_PATH}/item/wiring/wireitem.hpp"
    "${SRC_PATH}/item/wiring/splitteritem.hpp"
    "${SRC_PATH}/item/wiring/clockitem.hpp"
    "${SRC_PATH}/item/componentgraphicsitem.hpp"
    "${SRC_PATH}/item/compositeitem.hpp"
    "${SRC_PATH}/item/itemfactory.hpp"
    "${SRC_PATH}/util/colorutil.hpp"
    "${SRC_PATH}/util/connectionpoint.hpp"
    "${SRC_PATH}/util/direction.hpp"
    "${SRC_PATH}/util/wiredrawutils.hpp"
    "${SRC_PATH}/util/controlmode.hpp"
    "${SRC_PATH}/util/wiredrawinfo.hpp"
    "${SRC_PATH}/util/selectinfo.hpp"
    "${SRC_PATH}/util/misc.hpp"
    "${SRC_PATH}/util/clipboard.hpp"
    "${SRC_PATH}/mainwindow.hpp"
    "${SRC_PATH}/formuladialog.hpp"
    "${SRC_PATH}/scene.hpp"
)

set(UIS
    "${UI_PATH}/mainwindow.ui"
    "${UI_PATH}/formuladialog.ui"
)

set(RESOURCES
    "${RES_PATH}/resources.qrc"
)

set(CMAKE_AUTOUIC_SEARCH_PATHS ${UI_PATH})

add_executable(${PROJECT_NAME}
    ${SOURCES}
    ${HEADERS}
    ${UIS}
    ${RESOURCES}
)
target_link_libraries(${PROJECT_NAME} PRIVATE ${LIB_NAME} Qt${QT_VERSION_MAJOR}::Widgets)
target_include_directories(${PROJECT_NAME} PUBLIC "${SRC_PATH}")
